  /*
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "toposort.h"

List visit(Vertex vertex, List sorted, bool *temp, bool *perm, int i);
int check_bool_val(bool *perm, int order);
List source_search(Graph graph, List queue, int order);

/* Returns a list of topologically sorted vertices using the DFS method */
List dfs_sort(Graph graph) {
    List sorted = NULL;
    int i=0, order = graph->order;

    /* Create boolean arrays to mark nodes */
    bool temp[order];
    memset(temp,false,order);
    bool perm[order];
    memset(perm,false,order);

    /* If not permanently marked, visit node */
    while (check_bool_val(perm, order)){
        sorted = visit(&(graph->vertices[i]), sorted, temp, perm, i);
        i++;
    }

    return sorted;
}

List visit(Vertex vertex, List sorted, bool *temp, bool *perm, int i){
    List list = vertex->out;

    /* Cyclicity check */
    if (temp[i] == true){
      fprintf(stderr, "Error sorting vertices\n");
      exit(EXIT_FAILURE);
    }

    /* Check for permanently marked node */
    if (perm[i] == false){

        /* Mark n temporarily */
        temp[i] = true;

        /* Visit adjacent nodes*/
        while (list){
            sorted = (visit(list->data, sorted, temp, perm,
                     ((Vertex)list->data)->id));
            list = list->next;
        }
        /* Mark node permanently */
        perm[i] = true;

        /* Unmark node temporarily */
        temp[i] = false;

        /* If empty, create list and add item, otherwise update */
        if (sorted == NULL){
            sorted = push(NULL, &(vertex->id));
        } else {
            prepend(&(sorted), &(vertex->id));
        }
    }
    return sorted;
}

/* Check for false value in boolean array */
int check_bool_val(bool *perm, int order){
    int i;

    /* Return true if false value in array */
    for (i=0; i<order; i++){
        if(perm[i] == false){
            return true;
        }
    }
    return false;
}

/* Returns a list of topologically sorted vertices using the Kahn method */
List kahn_sort(Graph graph) {

    /* Declaration of variables */
    List list, sorted = NULL, queue = NULL;
    int order = graph->order;
    int size = graph->size;
    Vertex child, node;

    /* Store sources in queue */
    queue = source_search(graph, queue, order);

    /* Check for nodes in queue */
    while (queue){
        node = pop(&queue);
        prepend(&(sorted), node);
        list = node->out;

        /* Visit adjacent node */
        while (list){
            child = list->data;
            del(ptr_eq, node, &(child->in));
            size--;

            /* Check for incoming edges */
            if (child->in == NULL){
                prepend(&(queue), child);
            }
            list = list->next;
        }
    }

    /* Check for cyclicity */
    if (size!=0) {
      fprintf(stderr, "Error sorting vertices\n");
      exit(EXIT_FAILURE);
    } else{
      return reverse(sorted);
    }
}

List source_search(Graph graph, List queue, int order){
    int i;

    /* Searching for nodes without incoming edges */
    for (i=0; i<order; i++){
        if (graph->vertices[i].in == NULL){
              prepend(&(queue), graph->vertices+i);
        }
    }
    return queue;
}

/* Uses graph to verify vertices are topologically sorted */
bool verify(Graph graph, List sorted) {

    /* Declaration of variables */
    Vertex node;
    List list, items;
    int i, j, order = graph->order;

    /*  */
    for(i=0; i<order; i++){
        node = graph->vertices+i;
        list = node->out;
        items = sorted;

        /* Visit previous nodes */
        for(j=0;j<i;j++){

            /* Check for inequivalent nodes */
            if (find(ptr_eq, items->data, list)){
                return false;
            }
            items = items->next;
        }
    }

    return true;
}
