/*
 *
*/
#include <stdlib.h>
#include <assert.h>

#include "graph.h"

/* Returns a pointer to a new graph with order vertices */
Graph new_graph(int order) {

    /* Create graph */
    Graph graph;
    graph = malloc(sizeof(*graph)*order);
    assert(graph);

    /* Initialize graph */
    graph->size = 0;
    graph->order = order;
    graph->vertices = malloc(sizeof(*(graph->vertices))*order);
    assert(graph->vertices);

    return graph;
}

/* Returns whether aim and vertex are pointing to the same location */
bool ptr_eq(void *aim, void *vertex) {
  if (aim == vertex){
    return true;
  } else{
    return false;
  }
}

/* Returns whether aim and vertex have the same id */
bool id_eq(void *aim, void *vertex) {
    if (((Vertex)aim)->id == ((Vertex)vertex)->id){
      return true;
    } else{
      return false;
    }
}

/* Add the edge from v1 to v2 to graph */
void add_edge(Graph graph, int v1, int v2) {

  if (graph->vertices[v1].out == NULL){
    /* Empty, create list and add destination */
    graph->vertices[v1].out = push(NULL, &(graph->vertices[v2]));
  } else{
    /* Not empty, update destination */
    prepend(&(graph->vertices[v1].out), &(graph->vertices[v2]));
  }

  if (graph->vertices[v2].in == NULL){
    /* Empty, create list and add source */
    graph->vertices[v2].in = push(NULL, &(graph->vertices[v1]));
  } else{
    /* Not empty, update source*/
    prepend(&(graph->vertices[v2].in), &(graph->vertices[v1]));
  }

  /* Count number of edges */
  (graph->size)++;
}

/* Free the memory allocated to graph */
void free_graph(Graph graph) {
    free_list(graph->vertices->out);
    free_list(graph->vertices->in);
    free(graph->vertices);
    free(graph);
}
