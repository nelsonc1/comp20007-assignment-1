import re
from collections import defaultdict

paser = re.compile('(DFS|Kahn) sort: ([0-9]+) vertices, ([0-9]+) edges, ([0-9]+) clock ticks')
lst = []
while True:
    try:
        inp = input()
        m = paser.findall(inp)
        lst.append(m[0])
    except EOFError:
        break
ver_dict = defaultdict(dict)
for item in lst:
    print(item[1], item[2], item[3], item[4], sep=',')
