#!/bin/bash

ORDER=10000
for n in 10 20 30 40 50 60 70 80 90
do
    ./daggen $ORDER $n > test/$ORDER-$n.txt
	./ass1 -m 1 test/$ORDER-$n.txt > test/$ORDER-$n-1.out
	./ass1 -m 2 test/$ORDER-$n.txt > test/$ORDER-$n-2.out
done
