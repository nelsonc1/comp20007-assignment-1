/*
 * COMP20007 Design of Algorithms
 * Semester 1 2016
 *
 * Clement Poh (cpoh@unimelb.edu.au)
 *
 * This module provides all the IO functionality related to graphs.
 *
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "graphio.h"

#define MAX_LINE_LEN 256

/* Loads the graph from input */
Graph load_graph(char *input) {

    /* Initialization of variables */
    FILE *fp;
    Graph graph;
    char strn[MAX_LINE_LEN];
    char *node;
    int i, v1, v2;
    int order;

    fp = fopen(input, "r");
    /* If input file not found, exit */
    if (fp == NULL){
        printf("Error reading file\n");
        exit(1);
    }

    /* Get number of vertices */
    fgets(strn, MAX_LINE_LEN, fp);
    strtok(strn, "\n");
    order = atoi(strn);

    /* Create empty graph */
    graph = new_graph(order);


    /* Store id and labels from input into graph */
    for (i=0; i<order; i++){
        fgets(strn, MAX_LINE_LEN, fp);
        node = strtok(strn, "\n");
        graph->vertices[i].id = i;

        /* Creating memory space for label */
        graph->vertices[i].label = malloc(strlen(node)+1);
        assert(graph->vertices[i].label);

        strcpy(graph->vertices[i].label, node);
    }

    /* Connect graph with edges according to order from input*/
    while(fgets(strn, MAX_LINE_LEN, fp)){
        node = strtok(strn, "\n");
        v1 = atoi(strtok(node, " "));
        node = strtok(NULL, " ");
        v2 = atoi(strtok(node, " "));

        add_edge(graph, v1, v2);
    }

    return graph;
}

/* Prints the graph */
void print_graph(char *output, Graph graph) {
    FILE *f_write;
    int i=0, order = graph->order;

    /* Open output file */
    f_write = fopen(output, "w");

    fprintf(f_write, "digraph {\n");

    /* Prints label in order of vertices provided */
    while(i<order){
      fprintf(f_write, " ");
        /* Prints source vertices */
        print_vertex_label(f_write, &(graph->vertices[i]));

        /* If exist, prints destination vertices */
        if(graph->vertices[i].out != NULL){
            fprintf(f_write, "-> {");
            print_list(print_vertex_label, f_write, graph->vertices[i].out);
            fprintf(f_write, "}");
        }

        fprintf(f_write, "\n");
        i++;
    }

    fprintf(f_write, "}");
    fclose(f_write);
}


/* Prints the destination vertex label surrounded by spaces */
void print_vertex_label(FILE *file, void *vertex) {
    if ((Vertex)vertex)
        fprintf(file, " %s ", ((Vertex)vertex)->label);

}

/* Prints the id of a vertex then a newline */
void print_vertex_id(FILE *file, void *vertex) {
    if (vertex)
        fprintf(file, "%d\n", ((Vertex)vertex)->id);
}

/* Returns a sequence of vertices read from file */
List load_vertex_sequence(FILE *file, Graph graph) {
    const char *err_duplicate = "Error: duplicate vertex %d %s\n";
    const char *err_order = "Error: graph order %d, loaded %d vertices\n";
    List list = NULL;
    int id;

    while(fscanf(file, "%d\n", &id) == 1) {
        assert(id >= 0);
        assert(id < graph->order);

        if (!insert_if(id_eq, graph->vertices + id, &list)) {
            fprintf(stderr, err_duplicate, id, graph->vertices[id].label);
            exit(EXIT_FAILURE);
        }
    }

    if (len(list) != graph->order) {
        fprintf(stderr, err_order, graph->order, len(list));
        exit(EXIT_FAILURE);
    }

    return list;
}
